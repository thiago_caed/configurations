MENSAGES
---

# DEV

### orquestrador-exportar-entidade-parse.yml
``` json
{
  "id": null,
  "programa": "1123",
  "origem": "192.168.40.16:50084",
  "aplicacao": "repositorio-web",
  "evento": "ORQUESTRADOR_EXPORTAR_PARSE_FILTRO",
  "mensagem": {
    "fonte": "67",
    "nomeSequencia": "orquestrador-exportar-entidade-parse",
    "idConfiguracao": "5babb5a52c83d20f6f0b33ca"
  }
}
```

### orquestrador-migracao-dados-entidade
``` json
{
  "id": null,
  "programa": "1004",
  "origem": "192.168.40.16:50084",
  "aplicacao": "repositorio-web",
  "evento": "ORQUESTRADOR_IMPORTAR_ENTIDADE",
  "mensagem": {
    "fonte": "16",
    "nomeSequencia": "orquestrador-migracao-dados-entidade",
    "idConfiguracao": "5babb6f22c83d20f6f0b33cb"
  }
}
```

##orquestrador-aplicar-macro-resultado

### ORQUESTRADOR_GERAR_RESULTADO_PARTICIPACAO_REDE_PUBLICA
``` json
{
  "id": null,
  "programa": "1065",
  "origem": "192.168.40.16:50084",
  "aplicacao": "repositorio-web",
  "evento": "ORQUESTRADOR_GERAR_RESULTADO_PARTICIPACAO_REDE_PUBLICA",
  "mensagem": {
    "fonte": "72",
    "nomeSequencia": "orquestrador-aplicar-macro-resultado",
    "idConfiguracao": "5babb5772c83d20f6f0b33c9"
  }
}
```

### ORQUESTRADOR_GERAR_RESULTADO_PARTICIPACAO_REDE_ESTADUAL
``` json
{
  "id": null,
  "programa": "1065",
  "origem": "192.168.40.16:50084",
  "aplicacao": "repositorio-web",
  "evento": "ORQUESTRADOR_GERAR_RESULTADO_PARTICIPACAO_REDE_ESTADUAL",
  "mensagem": {
    "fonte": "72",
    "nomeSequencia": "orquestrador-aplicar-macro-resultado",
    "idConfiguracao": "5baa8e762c83d20f6f0b33c4"
  }
}
```

### ORQUESTRADOR_GERAR_RESULTADO_PARTICIPACAO_REDE_MUNICIPAL
``` json
{
  "id": null,
  "programa": "1065",
  "origem": "192.168.40.16:50084",
  "aplicacao": "repositorio-web",
  "evento": "ORQUESTRADOR_GERAR_RESULTADO_PARTICIPACAO_REDE_MUNICIPAL",
  "mensagem": {
    "fonte": "72",
    "nomeSequencia": "orquestrador-aplicar-macro-resultado",
    "idConfiguracao": "5ba8e8fc197cdf5df8138b3b"
  }
}
```

### GERAR_RESULTADO_TURMAS_MUNICIPAIS
``` json
{
  "id": null,
  "programa": "1065",
  "origem": "192.168.40.16:50084",
  "aplicacao": "repositorio-web",
  "evento": "ORQUESTRADOR_GERAR_RESULTADO_TURMAS_MUNICIPAIS",
  "mensagem": {
    "fonte": "72",
    "nomeSequencia": "orquestrador-aplicar-macro-resultado",
    "idConfiguracao": "5babe8802c83d20f6f0b33cc"
  }
}                    
```

### NUMERO EFETIVO DE TURMAS NAS REDES ESTADUAIS
``` json
{
  "id": null,
  "programa": "1065",
  "origem": "192.168.40.16:50084",
  "aplicacao": "repositorio-web",
  "evento": "ORQUESTRADOR_GERAR_RESULTADO_TURMAS_ESTADUAIS",
  "mensagem": {
    "fonte": "72",
    "nomeSequencia": "orquestrador-aplicar-macro-resultado",
    "idConfiguracao": "5bacdd1d2c83d20f6f0b33cf"
  }
}                    
```

### NUMERO EFETIVO DE TURMAS NA REDE PUBLICA
``` json
{
  "id": null,
  "programa": "1065",
  "origem": "192.168.40.16:50084",
  "aplicacao": "repositorio-web",
  "evento": "ORQUESTRADOR_GERAR_RESULTADO_TURMAS_REDE_PUBLICA",
  "mensagem": {
    "fonte": "72",
    "nomeSequencia": "orquestrador-aplicar-macro-resultado",
    "idConfiguracao": "5bacdf9a2c83d20f6f0b33d0"
  }
}                    
```

### NUMERO EFETIVO DE MEDIADORES NAS REDES MUNICIPAIS
``` json
{
  "id": null,
  "programa": "1065",
  "origem": "192.168.40.16:50084",
  "aplicacao": "repositorio-web",
  "evento": "ORQUESTRADOR_GERAR_RESULTADO_MEDIADORES_MUNICIPAIS",
  "mensagem": {
    "fonte": "72",
    "nomeSequencia": "orquestrador-aplicar-macro-resultado",
    "idConfiguracao": "5bace72e2c83d20f6f0b33d2"
  }
}                    
```

### NUMERO EFETIVO DE MEDIADORES NAS REDES ESTADUAIS
``` json
{
  "id": null,
  "programa": "1065",
  "origem": "192.168.40.16:50084",
  "aplicacao": "repositorio-web",
  "evento": "ORQUESTRADOR_GERAR_RESULTADO_MEDIADORES_ESTADUAL",
  "mensagem": {
    "fonte": "72",
    "nomeSequencia": "orquestrador-aplicar-macro-resultado",
    "idConfiguracao": "5bacf5f52c83d20f6f0b33d3"
  }
}                    
```

### NUMERO EFETIVO DE MEDIADORES NA REDE PUBLICA
``` json
{
  "id": null,
  "programa": "1065",
  "origem": "192.168.40.16:50084",
  "aplicacao": "repositorio-web",
  "evento": "ORQUESTRADOR_GERAR_RESULTADO_MEDIADORES_REDE_PUBLICA",
  "mensagem": {
    "fonte": "72",
    "nomeSequencia": "orquestrador-aplicar-macro-resultado",
    "idConfiguracao": "5bad0f322c83d20f6f0b33d4"
  }
}                    
```

### NUMERO EFETIVO DE FACILITADORES NA REDE PUBLICA
``` json
{
  "id": null,
  "programa": "1065",
  "origem": "192.168.40.16:50084",
  "aplicacao": "repositorio-web",
  "evento": "ORQUESTRADOR_GERAR_RESULTADO_FACILITADORES_REDE_PUBLICA",
  "mensagem": {
    "fonte": "72",
    "nomeSequencia": "orquestrador-aplicar-macro-resultado",
    "idConfiguracao": "5bad114d2c83d20f6f0b33d5"
  }
}                    
```

### NUMERO EFETIVO DE FACILITADORES NAS REDES ESTADUAIS
``` json
{
  "id": null,
  "programa": "1065",
  "origem": "192.168.40.16:50084",
  "aplicacao": "repositorio-web",
  "evento": "ORQUESTRADOR_GERAR_RESULTADO_FACILITADORES_REDE_ESTADUAL",
  "mensagem": {
    "fonte": "72",
    "nomeSequencia": "orquestrador-aplicar-macro-resultado",
    "idConfiguracao": "5bad13b52c83d20f6f0b33d6"
  }
}                    
```

### NUMERO EFETIVO DE FACILITADORES NAS REDES MUNICIPAIS
``` json
{
  "id": null,
  "programa": "1065",
  "origem": "192.168.40.16:50084",
  "aplicacao": "repositorio-web",
  "evento": "ORQUESTRADOR_GERAR_RESULTADO_FACILITADORES_REDE_MUNICIPAL",
  "mensagem": {
    "fonte": "72",
    "nomeSequencia": "orquestrador-aplicar-macro-resultado",
    "idConfiguracao": "5bad15a92c83d20f6f0b33d7"
  }
}                    
```

### NUMERO EFETIVO DE TROCAS DE MEDIADORES NAS REDES ESTADUAIS
``` json
{
  "id": null,
  "programa": "1065",
  "origem": "192.168.40.16:50084",
  "aplicacao": "repositorio-web",
  "evento": "ORQUESTRADOR_GERAR_RESULTADO_TROCAS_MEDIADORES_ESTADUAIS",
  "mensagem": {
    "fonte": "72",
    "nomeSequencia": "orquestrador-aplicar-macro-resultado",
    "idConfiguracao": "5bad2f2b2c83d20f6f0b33d8"
  }
}                    
```

### NUMERO EFETIVO DE TROCAS DE MEDIADORES NAS REDES MUNICIPAIS
``` json
{
  "id": null,
  "programa": "1065",
  "origem": "192.168.40.16:50084",
  "aplicacao": "repositorio-web",
  "evento": "ORQUESTRADOR_GERAR_RESULTADO_TROCAS_MEDIADORES_MUNICIPAIS",
  "mensagem": {
    "fonte": "72",
    "nomeSequencia": "orquestrador-aplicar-macro-resultado",
    "idConfiguracao": "5bad2f332c83d20f6f0b33d9"
  }
}                    
```

### NUMERO EFETIVO DE TROCAS DE MEDIADORES NA REDE PUBLICA
``` json
{
  "id": null,
  "programa": "1065",
  "origem": "192.168.40.16:50084",
  "aplicacao": "repositorio-web",
  "evento": "ORQUESTRADOR_GERAR_RESULTADO_TROCAS_MEDIADORES_ESTADUAIS",
  "mensagem": {
    "fonte": "72",
    "nomeSequencia": "orquestrador-aplicar-macro-resultado",
    "idConfiguracao": "5bad2f3b2c83d20f6f0b33da"
  }
}                    
```

### NUMERO EFETIVO DE TROCAS DE FACILITADORES NAS REDES ESTADUAIS
``` json
{
  "id": null,
  "programa": "1065",
  "origem": "192.168.40.16:50084",
  "aplicacao": "repositorio-web",
  "evento": "ORQUESTRADOR_GERAR_RESULTADO_TROCAS_FACILITADORES_ESTADUAIS",
  "mensagem": {
    "fonte": "72",
    "nomeSequencia": "orquestrador-aplicar-macro-resultado",
    "idConfiguracao": "5bad423f2c83d20f6f0b33dc"
  }
}                    
```

### NUMERO EFETIVO DE TROCAS DE FACILITADORES NAS REDES MUNICIPAIS
``` json
{
  "id": null,
  "programa": "1065",
  "origem": "192.168.40.16:50084",
  "aplicacao": "repositorio-web",
  "evento": "ORQUESTRADOR_GERAR_RESULTADO_TROCAS_FACILITADORES_MUNICIPAIS",
  "mensagem": {
    "fonte": "72",
    "nomeSequencia": "orquestrador-aplicar-macro-resultado",
    "idConfiguracao": "5bad42452c83d20f6f0b33dd"
  }
}                    
```

### NUMERO EFETIVO DE TROCAS DE FACILITADORES  NA REDE PUBLICA
``` json
{
  "id": null,
  "programa": "1065",
  "origem": "192.168.40.16:50084",
  "aplicacao": "repositorio-web",
  "evento": "ORQUESTRADOR_GERAR_RESULTADO_TROCAS_FACILITADORES_PUBLICA",
  "mensagem": {
    "fonte": "72",
    "nomeSequencia": "orquestrador-aplicar-macro-resultado",
    "idConfiguracao": "5bad424b2c83d20f6f0b33de"
  }
}                    
```

# HMG
orquestrador-aplicar-macro-resultado
``` json
{
  "programa": "1123",
  "origem": "qualquer origem",
  "aplicacao": "novo velho mais educacao",
  "evento": "ORQUESTRADOR_APLICAR_MACRO",
  "mensagem": {
    "fonte": "16",
    "nomeSequencia": "orquestrador-aplicar-macro-resultado",
    "idConfiguracao": "5b995e086a3d8466b4f3c2f9"
  }
}
```

orquestrador-migracao-dados-entidade
``` json
{
  "programa": "1004",
  "origem": "qualquer origem",
  "aplicacao": "novo velho mais educacao",
  "evento": "MIGRAR_DADOS",
  "mensagem": {
    "fonte": "16",
    "nomeSequencia": "orquestrador-migracao-dados-entidade",
    "idConfiguracao": "5b9fb60f19a36f5d9b4f675a"
  }
}
```

orquestrador-exportar-entidade-parse
``` json
{
  "programa": "1126",
  "origem": "qualquer origem",
  "aplicacao": "novo velho mais educacao",
  "evento": "ORQUESTRADOR_IMPORTAR_ENTIDADE",
  "mensagem": {
    "fonte": "90",
    "nomeSequencia": "orquestrador-exportar-entidade-parse",
    "idConfiguracao": "5b9ab8b36a3d8466b4f3c41c"
  }
}
```
