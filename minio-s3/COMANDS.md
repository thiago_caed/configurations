MINIOS3
---

### Configure MC - hmg
mc config host add s3 http://10.0.10.57:8600 minio-adm-hmg xxx

### Configure MC - prod
mc config host add s3-prod http://192.168.40.16:48500 minio-adm-prod xxx


### Policy
mc admin policy list s3-prod
mc admin policy remove s3-prod readonlypolicy

### Criar policy
mc admin policy add s3-prod readonly readonly.json

### User
mc admin user list s3-prod 
mc admin user remove s3-prod readonlyuser

### Criar usuario
mc admin user add s3-prod repositorio xxx readonly
mc admin user add s3-prod operacoes xxx readonly
mc admin user add s3-prod renatomacedo xxx readonly

### Listar 
mc ls s3-prod


## Access Policy
https://github.com/minio/minio/tree/master/docs/bucket/policy