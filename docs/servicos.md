Repositorio
---

## Servicos Atuais
### Infra
- ZooKeeper
- Kafka
- Redis
- MongoDb
- S3
- ElasticSearch
- Logstash
- Kibana

### Servicos
- Orquestrador
- Execution
- Configuracoes
- Formulario
- SyncProxy
- Sync
- FileBatchConnector
- FileBatch
- TransformerEventAnalyser
- TransformerAnalyser
- TransformerRawData
- FileProcessor
- Resultado
- MongoConnector
- DataRawConnector
- AnalysisEngine
- DeleterExtractor
- DataRawSink
- EventSyncProxy

## Mudanças

### Infra
- ZooKeeper (Escalar)
- Kafka (Escalar) 
- Redis (Escalar)
- MongoDb
- S3
- ElasticSearch (Escalar)
- Logstash (Sync)
- Logstash (Logs)
- Logstash (Monitoring)
- Kibana

### Server
- FN (Server)
- FNController (Service) 

### IntegrationFlowsServices
- Orquestrador (FN)
- Execution (FN)
- Scheduler (Service)
- Execution (FN)
- Configuracoes (FN)
- Formulario (FN)

### IntegrationFlows
- FileProcessor (FN)
- FileWriter (Escrever S3, FN)
- Resultado (Service)
- DataRawConnector (Extractor)(FN)
- DeleterExtractor (Extractor)(FN)
- DataRawSink (Consumer) (FN)
- Transformer (STR)
    - TransformerRawData (STR)
    - TransformerEventAnalyser (STR)
    - TransformerAnalyser (STR)
- Dics (FN)
  
### Events  
- EventSyncProxy (FN)
- AnalysisEngine (Service)

### Sync
- SyncProxy (Service)
- Sync (STR)

### Repositorio
- FileBatchConnector (Service)
- FileBatch (Batch)

---

