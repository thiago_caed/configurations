#!/usr/bin/env bash

docker rm -f elasticsearch

docker run -d \
    --name elasticsearch \
    -p 9200:9200 \
    -p 9300:9300 \
    ccr.caeddigital.net/repositorio/servers/elasticsearch:1.0.1

docker rm -f graylog

docker run --link kafka-1:kafka --link zookeeper:zookeeper --link redis:redis --link mongo:mongo --link elasticsearch:elasticsearch \
    -p 9000:9000 -p 12201:12201 -p 514:514 \
    --name graylog \
    -e GRAYLOG_WEB_ENDPOINT_URI="http://127.0.0.1:9000/api" \
    -d graylog-with-sso-plugin