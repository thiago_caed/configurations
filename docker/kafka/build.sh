#!/usr/bin/env bash

#Build
docker build -t ccr.caeddigital.net/repositorio/servers/kafka .
docker tag ccr.caeddigital.net/repositorio/servers/zookeeper ccr.caeddigital.net/repositorio/servers/kafka:1.2

#Login
docker login -u $1 --password-stdin $2 ccr.caeddigital.net

docker push ccr.caeddigital.net/repositorio/servers/kafka:1.2