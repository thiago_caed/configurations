MONGOCLUSTER 
---

SERVER 1
docker rm -f $(docker ps -a -q) &&  sudo rm -R data && mkdir data

docker run --name mongo -v /home/pablo/workspace/projects/configurations/docker/mongo/data:/data/db --hostname="node1.db" -p 27017:27017 -d mongocluster --smallfiles

docker exec -it mongo /bin/bash
mongo
use admin

db.createUser( {
     user: "admin",
     pwd: "SENHA_DE_ADMIN",
     roles: [ { role: "userAdminAnyDatabase", db: "admin" } ]
   });
db.createUser( {
     user: "root",
     pwd: "SENHA_DE_ROOT",
     roles: [ { role: "root", db: "admin" } ]
   });
exit
exit
docker rm -f mongo

docker run --name mongo -v /home/pablo/workspace/projects/configurations/docker/mongo/data:/data/db --hostname="node1.db" --add-host node1.db:172.17.0.1 --add-host node2.db:172.17.0.1 --add-host node3.db:172.17.0.1 -p 27017:27017 -d mongocluster --smallfiles --keyFile /opt/keyfile/mongodb-keyfile --replSet "rs0"

docker exec -it mongo /bin/bash
mongo
use admin
db.auth("root", "SENHA_DE_ROOT");
rs.initiate()
rs.conf()

---
SERVER 2
mkdir data2
docker run --name mongo-2 -v /home/pablo/workspace/projects/configurations/docker/mongo/data2:/data/db --hostname="node2.db" -p 27018:27017 -d mongocluster --smallfiles --keyFile /opt/keyfile/mongodb-keyfile --replSet "rs0"

SERVER 3
mkdir data3
docker run --name mongo-3 -v /home/pablo/workspace/projects/configurations/docker/mongo/data3:/data/db --hostname="node2.db" -p 27019:27017 -d mongocluster --smallfiles --keyFile /opt/keyfile/mongodb-keyfile --replSet "rs0"


PARA O SERVER1
docker exec -it mongo /bin/bash


---
docker run -it --name mongo_1 --net=host -v /home/pablo/workspace/projects/configurations/docker/mongo/data1:/data/db mongo

docker exec -it mongo_1 bash
openssl rand -base64 756 > mongo-keyfile

kdir /opt/mongo
mv mongo-keyfile /opt/mongo/
chmod 400 /opt/mongo/mongo-keyfile
chown mongodb:mongodb /opt/mongo/mongo-keyfile

mongo
use admin
db.createUser({user: "mongo-admin", pwd: "password", roles:[{role: "root", db: "admin"}]})
