
sudo rm -R /home/pablo/workspace/projects/configurations/docker/mongo/data1 &&  mkdir -p /home/pablo/workspace/projects/configurations/docker/mongo/data1 &&  sudo rm -R /home/pablo/workspace/projects/configurations/docker/mongo/data2 && mkdir -p /home/pablo/workspace/projects/configurations/docker/mongo/data2 && sudo rm -R /home/pablo/workspace/projects/configurations/docker/mongo/data3 && mkdir -p /home/pablo/workspace/projects/configurations/docker/mongo/data3

#docker run --name mongo1 -v /home/pablo/workspace/projects/configurations/docker/mongo/data1:/data/db --hostname="node1.db" -p 27017:27017 -d mongocluster --smallfiles
#docker exec -it mongo1 /bin/bash
#mongo
#use admin
#db.createUser( {     user: "admin",     pwd: "SENHA_DE_ADMIN",     roles: [ { role: "userAdminAnyDatabase", db: "admin" } ]   });
#db.createUser( {     user: "root",     pwd: "SENHA_DE_ROOT",     roles: [ { role: "root", db: "admin" } ]   });
#exit
#docker rm -f mongo1

docker run --name mongo1 -v /home/pablo/workspace/projects/configurations/docker/mongo/data1:/data/db --hostname="node1.db" \
    --add-host node1.db:172.17.0.1 --add-host node2.db:172.17.0.1 --add-host node3.db:172.17.0.1 \
    --add-host node1.cfg:172.17.0.1 --add-host node2.cfg:172.17.0.1 --add-host node3.cfg:172.17.0.1 \
    --add-host node1.router:172.17.0.1 \
    -d mongocluster mongod --shardsvr  --noprealloc --smallfiles --keyFile /opt/keyfile/mongodb-keyfile --replSet "rs0" --port 27017

docker run --name mongo2 -v /home/pablo/workspace/projects/configurations/docker/mongo/data2:/data/db --hostname="node2.db" \
    --add-host node1.db:172.17.0.1 --add-host node2.db:172.17.0.1 --add-host node3.db:172.17.0.1 \
    --add-host node1.cfg:172.17.0.1 --add-host node2.cfg:172.17.0.1 --add-host node3.cfg:172.17.0.1 \
    --add-host node1.router:172.17.0.1 \
    -d mongocluster mongod --shardsvr  --noprealloc --smallfiles --keyFile /opt/keyfile/mongodb-keyfile --replSet "rs0" --port 27017


docker run --name mongo3 -v /home/pablo/workspace/projects/configurations/docker/mongo/data3:/data/db --hostname="node3.db" \
    --add-host node1.db:172.17.0.1 --add-host node2.db:172.17.0.1 --add-host node3.db:172.17.0.1 \
    --add-host node1.cfg:172.17.0.1 --add-host node2.cfg:172.17.0.1 --add-host node3.cfg:172.17.0.1 \
    -d mongocluster mongod --shardsvr  --noprealloc --smallfiles --keyFile /opt/keyfile/mongodb-keyfile --replSet "rs0" --port 27017

--- 

docker exec -it mongo1 /bin/bash
mongo
use admin
#db.auth("root", "SENHA_DE_ROOT");
#rs.initiate({_id : "rs0", members: [{ _id : 0, host : "node1.db" }, { _id : 1, host : "node2.db"}, { _id : 2, host : "node2.db" } ] } )
rs.initiate({_id : "rs0", members: [{ _id : 0, host : "172.17.0.2" }, { _id : 1, host : "172.17.0.3"}, { _id : 2, host : "172.17.0.4" } ] } )

rs.conf()   

---

sudo rm -R /home/pablo/workspace/projects/configurations/docker/mongo/data4 && mkdir -p /home/pablo/workspace/projects/configurations/docker/mongo/data4 && sudo rm -R /home/pablo/workspace/projects/configurations/docker/mongo/data5 && mkdir -p /home/pablo/workspace/projects/configurations/docker/mongo/data5 && sudo rm -R /home/pablo/workspace/projects/configurations/docker/mongo/data6 && mkdir -p /home/pablo/workspace/projects/configurations/docker/mongo/data6

docker run --name cfg1 -v /home/pablo/workspace/projects/configurations/docker/mongo/data4:/data/db --hostname="node1.cfg" \
    --add-host node1.db:172.17.0.1 --add-host node2.db:172.17.0.1 --add-host node3.db:172.17.0.1 \
    --add-host node1.cfg:172.17.0.1 --add-host node2.cfg:172.17.0.1 --add-host node3.cfg:172.17.0.1 \
    --add-host node1.router:172.17.0.1 \
    -d mongocluster mongod --configsvr  --noprealloc --smallfiles --keyFile /opt/keyfile/mongodb-keyfile --replSet "rscf0" --port 27017

docker run --name cfg2 -v /home/pablo/workspace/projects/configurations/docker/mongo/data5:/data/db --hostname="node2.cfg" \
    --add-host node1.db:172.17.0.1 --add-host node2.db:172.17.0.1 --add-host node3.db:172.17.0.1 \
    --add-host node1.cfg:172.17.0.1 --add-host node2.cfg:172.17.0.1 --add-host node3.cfg:172.17.0.1 \
    --add-host node1.router:172.17.0.1 \
    -d mongocluster mongod --configsvr  --noprealloc --smallfiles --keyFile /opt/keyfile/mongodb-keyfile --replSet "rscf0" --port 27017

docker run --name cfg3 -v /home/pablo/workspace/projects/configurations/docker/mongo/data6:/data/db --hostname="node3.cfg" \
    --add-host node1.db:172.17.0.1 --add-host node2.db:172.17.0.1 --add-host node3.db:172.17.0.1 \
    --add-host node1.cfg:172.17.0.1 --add-host node2.cfg:172.17.0.1 --add-host node3.cfg:172.17.0.1 \
    --add-host node1.router:172.17.0.1 \
    -d mongocluster mongod --configsvr  --noprealloc --smallfiles --keyFile /opt/keyfile/mongodb-keyfile --replSet "rscf0" --port 27017
    
docker exec -it cfg1 bash
mongo
use admin
rs.initiate({_id : "rscf0", members: [{ _id : 0, host : "172.17.0.5" }, { _id : 1, host : "172.17.0.6"}, { _id : 2, host : "172.17.0.7" } ] } )

    
----     

docker run --name route1 --net=host -d mongocluster mongos --keyFile /opt/keyfile/mongodb-keyfile --configdb rscf0/172.17.0.5:27017,172.17.0.6:27017,172.17.0.7:27017 --port 27017

docker exec -it route1 bash
mongo
use admin    
db.createUser({user: "dbadmin", pwd: "YV2PB7X9", roles:[{ role:"dbOwner",db:"admin" },{ role: "root", db: "admin" }]})

db.auth("dbadmin", "YV2PB7X9");
db.createUser({ user: "repositorioAdm", pwd: "YV2PB7X9", roles: [{ role: "userAdminAnyDatabase", db: "admin" } ,{ role: "root", db: "admin" }] })

use admin
db.auth("repositorioAdm", "YV2PB7X9");
sh.status()
sh.addShard("rs0/172.17.0.2:27017")
sh.addShard("rs0/172.17.0.3:27017")
sh.addShard("rs0/172.17.0.4:27017")
sh.status()

use interno
db.createUser({ user: "repositorioUser", pwd: "YV2PB7X9", roles: [ { role:"dbOwner",db:"interno" } , { role: "readWrite", db: "interno"}, {role: "dbAdmin", db: "interno"} ] })
sh.enableSharding("interno")

use interno
db.auth("repositorioUser", "YV2PB7X9");
db.createCollection("teste")
sh.shardCollection("interno.teste", {"_id" : 1})
db.getCollection("teste").getShardDistribution();



for (var i = 1; i <= 500; i++) db.teste.insert( { x : i } )

