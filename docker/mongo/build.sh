#!/usr/bin/env bash

#Build
docker build -t ccr.caeddigital.net/repositorio/servers/mongocluster .
docker tag ccr.caeddigital.net/repositorio/servers/mongocluster ccr.caeddigital.net/repositorio/servers/mongocluster:1.0

#Login
docker login -u $1 --password-stdin $2 ccr.caeddigital.net

docker push ccr.caeddigital.net/repositorio/servers/mongocluster:1.0