docker rm -f $(docker ps -a -q)
sudo rm -R /home/pablo/workspace/projects/configurations/docker/mongo/data1 &&  mkdir -p /home/pablo/workspace/projects/configurations/docker/mongo/data1 &&  sudo rm -R /home/pablo/workspace/projects/configurations/docker/mongo/data2 && mkdir -p /home/pablo/workspace/projects/configurations/docker/mongo/data2 && sudo rm -R /home/pablo/workspace/projects/configurations/docker/mongo/data3 && mkdir -p /home/pablo/workspace/projects/configurations/docker/mongo/data3

sleep 1

docker run --name mongo1 -v /home/pablo/workspace/projects/configurations/docker/mongo/data1:/data/db --hostname="node1.db" \
    --add-host node1.db:172.17.0.1 --add-host node2.db:172.17.0.1 --add-host node3.db:172.17.0.1 \
    --add-host node1.cfg:172.17.0.1 --add-host node2.cfg:172.17.0.1 --add-host node3.cfg:172.17.0.1 \
    --add-host node1.router:172.17.0.1 \
    --memory="128m" \
    -d mongocluster mongod --shardsvr  --noprealloc --smallfiles --keyFile /opt/keyfile/mongodb-keyfile --replSet "rs0" --port 27017

sleep 1

docker run --name mongo2 -v /home/pablo/workspace/projects/configurations/docker/mongo/data2:/data/db --hostname="node2.db" \
    --add-host node1.db:172.17.0.1 --add-host node2.db:172.17.0.1 --add-host node3.db:172.17.0.1 \
    --add-host node1.cfg:172.17.0.1 --add-host node2.cfg:172.17.0.1 --add-host node3.cfg:172.17.0.1 \
    --add-host node1.router:172.17.0.1 \
    --memory="128m" \
    -d mongocluster mongod --shardsvr  --noprealloc --smallfiles --keyFile /opt/keyfile/mongodb-keyfile --replSet "rs0" --port 27017

sleep 1

docker run --name mongo3 -v /home/pablo/workspace/projects/configurations/docker/mongo/data3:/data/db --hostname="node3.db" \
    --add-host node1.db:172.17.0.1 --add-host node2.db:172.17.0.1 --add-host node3.db:172.17.0.1 \
    --add-host node1.cfg:172.17.0.1 --add-host node2.cfg:172.17.0.1 --add-host node3.cfg:172.17.0.1 \
    --memory="128m" \
    -d mongocluster mongod --shardsvr  --noprealloc --smallfiles --keyFile /opt/keyfile/mongodb-keyfile --replSet "rs0" --port 27017

sleep 1

sudo rm -R /home/pablo/workspace/projects/configurations/docker/mongo/data4 && mkdir -p /home/pablo/workspace/projects/configurations/docker/mongo/data4 && sudo rm -R /home/pablo/workspace/projects/configurations/docker/mongo/data5 && mkdir -p /home/pablo/workspace/projects/configurations/docker/mongo/data5 && sudo rm -R /home/pablo/workspace/projects/configurations/docker/mongo/data6 && mkdir -p /home/pablo/workspace/projects/configurations/docker/mongo/data6

sleep 1

docker run --name cfg1 -v /home/pablo/workspace/projects/configurations/docker/mongo/data4:/data/db --hostname="node1.cfg" \
    --add-host node1.db:172.17.0.1 --add-host node2.db:172.17.0.1 --add-host node3.db:172.17.0.1 \
    --add-host node1.cfg:172.17.0.1 --add-host node2.cfg:172.17.0.1 --add-host node3.cfg:172.17.0.1 \
    --add-host node1.router:172.17.0.1 \
    --memory="128m" \
    -d mongocluster mongod --configsvr  --noprealloc --smallfiles --keyFile /opt/keyfile/mongodb-keyfile --replSet "rscf0" --port 27017

sleep 1

docker run --name cfg2 -v /home/pablo/workspace/projects/configurations/docker/mongo/data5:/data/db --hostname="node2.cfg" \
    --add-host node1.db:172.17.0.1 --add-host node2.db:172.17.0.1 --add-host node3.db:172.17.0.1 \
    --add-host node1.cfg:172.17.0.1 --add-host node2.cfg:172.17.0.1 --add-host node3.cfg:172.17.0.1 \
    --add-host node1.router:172.17.0.1 \
    --memory="128m" \
    -d mongocluster mongod --configsvr  --noprealloc --smallfiles --keyFile /opt/keyfile/mongodb-keyfile --replSet "rscf0" --port 27017

sleep 1

docker run --name cfg3 -v /home/pablo/workspace/projects/configurations/docker/mongo/data6:/data/db --hostname="node3.cfg" \
    --add-host node1.db:172.17.0.1 --add-host node2.db:172.17.0.1 --add-host node3.db:172.17.0.1 \
    --add-host node1.cfg:172.17.0.1 --add-host node2.cfg:172.17.0.1 --add-host node3.cfg:172.17.0.1 \
    --add-host node1.router:172.17.0.1 \
    --memory="128m" \
    -d mongocluster mongod --configsvr  --noprealloc --smallfiles --keyFile /opt/keyfile/mongodb-keyfile --replSet "rscf0" --port 27017

---

docker exec -it mongo1 mongo << EOF
rs.initiate({_id : "rs0", members: [{ _id : 0, host : "172.17.0.2" }, { _id : 1, host : "172.17.0.3"}, { _id : 2, host : "172.17.0.4" } ] } )
EOF


docker exec -it mongo1 /bin/bash
mongo
use admin
rs.initiate({_id : "rs0", members: [{ _id : 0, host : "172.17.0.2" }, { _id : 1, host : "172.17.0.3"}, { _id : 2, host : "172.17.0.4" } ] } )

docker exec -it cfg1 bash
mongo
use admin
rs.initiate({_id : "rscf0", members: [{ _id : 0, host : "172.17.0.5" }, { _id : 1, host : "172.17.0.6"}, { _id : 2, host : "172.17.0.7" } ] } )

docker run --name route1 --net=host -d mongocluster mongos --keyFile /opt/keyfile/mongodb-keyfile --configdb rscf0/172.17.0.5:27017,172.17.0.6:27017,172.17.0.7:27017 --port 27017
docker exec -it route1 bash
mongo
use admin    
db.createUser({user: "mongo-admin", pwd: "password", roles:[{role: "root", db: "admin"}]})

mongo -u mongo-admin -p "password" --authenticationDatabase admin
sh.addShard("rs0/172.17.0.2:27017")
sh.addShard("rs0/172.17.0.3:27017")
sh.addShard("rs0/172.17.0.4:27017")


use interno
sh.enableSharding("interno")

db.createCollection("teste")
db.teste.ensureIndex( { _id : "hashed" } )

sh.shardCollection( "interno.teste", { "_id" : "hashed" } )

for (var i = 1; i <= 500; i++) db.teste.insert( { x : i } )



db.teste.getShardDistribution()



----- 
CONFIGURE EM HMG

service-bus-hmg-v2-mongodb-1
mongo
use admin
rs.initiate({_id : "rs0", members: [{ _id : 0, host : "service-bus-hmg-v2-mongodb-1" }, { _id : 1, host : "service-bus-hmg-v2-mongodb-2"}, { _id : 2, host : "service-bus-hmg-v2-mongodb-3" } ] } )

service-bus-hmg-v2-mongocfg-1
mongo
use admin
rs.initiate({_id : "rscf0", members: [{ _id : 0, host : "service-bus-hmg-v2-mongocfg-1" }, { _id : 1, host : "service-bus-hmg-v2-mongocfg-2"}, { _id : 2, host : "service-bus-hmg-v2-mongocfg-3" } ] } )

service-bus-hmg-v2-mongorouter-1
mongo
use admin
db.createUser({user:"mongo-admin", pwd: "password", roles:[{role: "root", db: "admin"}]})

mongo -u mongo-admin -p "password" --authenticationDatabase admin
sh.addShard("rs0/service-bus-hmg-v2-mongodb-1:27017")
sh.addShard("rs0/service-bus-hmg-v2-mongodb-2:27017")
sh.addShard("rs0/service-bus-hmg-v2-mongodb-2:27017")