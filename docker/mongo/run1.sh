#!/usr/bin/env bash

docker rm -f $(docker ps -a -q)
sudo rm -R /home/pablo/workspace/projects/configurations/docker/mongo/data1 &&  mkdir -p /home/pablo/workspace/projects/configurations/docker/mongo/data1 &&  sudo rm -R /home/pablo/workspace/projects/configurations/docker/mongo/data2 && mkdir -p /home/pablo/workspace/projects/configurations/docker/mongo/data2 && sudo rm -R /home/pablo/workspace/projects/configurations/docker/mongo/data3 && mkdir -p /home/pablo/workspace/projects/configurations/docker/mongo/data3

sleep 1

docker run --name mongo1 -v /home/pablo/workspace/projects/configurations/docker/mongo/data1:/data/db --hostname="node1.db" \
    --add-host node1.db:172.17.0.2 --add-host node2.db:172.17.0.3 --add-host node3.db:172.17.0.4 \
    --add-host node1.cfg:172.17.0.5 --add-host node2.cfg:172.17.0.6 --add-host node3.cfg:172.17.0.7 \
    --add-host node1.router:172.17.0.1 \
    -d mongocluster mongod --shardsvr  --noprealloc --smallfiles --keyFile /opt/keyfile/mongodb-keyfile --replSet "rs0" --port 27017

sleep 1

docker run --name mongo2 -v /home/pablo/workspace/projects/configurations/docker/mongo/data2:/data/db --hostname="node2.db" \
    --add-host node1.db:172.17.0.2 --add-host node2.db:172.17.0.3 --add-host node3.db:172.17.0.4 \
    --add-host node1.cfg:172.17.0.5 --add-host node2.cfg:172.17.0.6 --add-host node3.cfg:172.17.0.7 \
    --add-host node1.router:172.17.0.1 \
    -d mongocluster mongod --shardsvr  --noprealloc --smallfiles --keyFile /opt/keyfile/mongodb-keyfile --replSet "rs0" --port 27017

sleep 1

docker run --name mongo3 -v /home/pablo/workspace/projects/configurations/docker/mongo/data3:/data/db --hostname="node3.db" \
    --add-host node1.db:172.17.0.2 --add-host node2.db:172.17.0.3 --add-host node3.db:172.17.0.4 \
    --add-host node1.cfg:172.17.0.5 --add-host node2.cfg:172.17.0.6 --add-host node3.cfg:172.17.0.7 \
    --add-host node1.router:172.17.0.1 \
    -d mongocluster mongod --shardsvr  --noprealloc --smallfiles --keyFile /opt/keyfile/mongodb-keyfile --replSet "rs0" --port 27017

sleep 1

sudo rm -R /home/pablo/workspace/projects/configurations/docker/mongo/data4 && mkdir -p /home/pablo/workspace/projects/configurations/docker/mongo/data4 && sudo rm -R /home/pablo/workspace/projects/configurations/docker/mongo/data5 && mkdir -p /home/pablo/workspace/projects/configurations/docker/mongo/data5 && sudo rm -R /home/pablo/workspace/projects/configurations/docker/mongo/data6 && mkdir -p /home/pablo/workspace/projects/configurations/docker/mongo/data6

sleep 1

docker run --name cfg1 -v /home/pablo/workspace/projects/configurations/docker/mongo/data4:/data/db --hostname="node1.cfg" \
    --add-host node1.db:172.17.0.2 --add-host node2.db:172.17.0.3 --add-host node3.db:172.17.0.4 \
    --add-host node1.cfg:172.17.0.5 --add-host node2.cfg:172.17.0.6 --add-host node3.cfg:172.17.0.7 \
    --add-host node1.router:172.17.0.1 \
    -d mongocluster mongod --configsvr  --noprealloc --smallfiles --keyFile /opt/keyfile/mongodb-keyfile --replSet "rscf0" --port 27017

sleep 1

docker run --name cfg2 -v /home/pablo/workspace/projects/configurations/docker/mongo/data5:/data/db --hostname="node2.cfg" \
    --add-host node1.db:172.17.0.2 --add-host node2.db:172.17.0.3 --add-host node3.db:172.17.0.4 \
    --add-host node1.cfg:172.17.0.5 --add-host node2.cfg:172.17.0.6 --add-host node3.cfg:172.17.0.7 \
    --add-host node1.router:172.17.0.1 \
    -d mongocluster mongod --configsvr  --noprealloc --smallfiles --keyFile /opt/keyfile/mongodb-keyfile --replSet "rscf0" --port 27017

sleep 1

docker run --name cfg3 -v /home/pablo/workspace/projects/configurations/docker/mongo/data6:/data/db --hostname="node3.cfg" \
    --add-host node1.db:172.17.0.2 --add-host node2.db:172.17.0.3 --add-host node3.db:172.17.0.4 \
    --add-host node1.cfg:172.17.0.5 --add-host node2.cfg:172.17.0.6 --add-host node3.cfg:172.17.0.7 \
    --add-host node1.router:172.17.0.1 \
    -d mongocluster mongod --configsvr  --noprealloc --smallfiles --keyFile /opt/keyfile/mongodb-keyfile --replSet "rscf0" --port 27017

sleep 1
docker exec -it mongo1 bash -c "echo 'rs.initiate({_id : \"rs0\", members: [{ _id : 0, host : \"node1.db\" }, { _id : 1, host : \"node2.db\"}, { _id : 2, host : \"node3.db\" } ] } )' | mongo"

sleep 1
docker exec -it cfg1 bash -c "echo 'rs.initiate({_id : \"rscf0\", members: [{ _id : 0, host : \"node1.cfg\" }, { _id : 1, host : \"node2.cfg\"}, { _id : 2, host : \"node3.cfg\" } ] } )' | mongo"

sleep 3
docker run --name route1 --net=host -d mongocluster mongos --keyFile /opt/keyfile/mongodb-keyfile --configdb rscf0/node1.cfg:27017,node2.cfg:27017,node3.cfg:27017 --port 27017

#sleep 10
#docker exec -it route1 bash -c "echo 'db.createUser({user:\"mongo-admin\", pwd: \"password\", roles:[{role: \"root\", db: \"admin\"}]})' | mongo admin"
#
#sleep 1
#docker exec -it route1 bash -c "echo 'sh.addShard(\"rs0/node1.db:27017\")' | mongo -u mongo-admin -p \"password\" --authenticationDatabase admin"
#sleep 1
#docker exec -it route1 bash -c "echo 'sh.addShard(\"rs0/node2.db:27017\")' | mongo -u mongo-admin -p \"password\" --authenticationDatabase admin"
#sleep 1
#docker exec -it route1 bash -c "echo 'sh.addShard(\"rs0/node3.db:27017\")' | mongo -u mongo-admin -p \"password\" --authenticationDatabase admin"

