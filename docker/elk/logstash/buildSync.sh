#!/usr/bin/env bash

REPOSITORY=servers;
IMAGE_NAME=logstash;
IMAGETAG=HMG-SYNC-1.0.1;

#Build
docker build -t ccr.caeddigital.net/repositorio/$REPOSITORY/$IMAGE_NAME .
#docker tag ccr.caeddigital.net/repositorio/$REPOSITORY/$IMAGE_NAME ccr.caeddigital.net/repositorio/$REPOSITORY/$IMAGE_NAME:$IMAGETAG

#Login
#docker login -u $1 --password-stdin $2 ccr.caeddigital.net

#docker push ccr.caeddigital.net/repositorio/$REPOSITORY/$IMAGE_NAME:$IMAGETAG

docker rm -f logstash
# docker run -it -p 5000:5000 --name=logstash ccr.caeddigital.net/repositorio/servers/logstash