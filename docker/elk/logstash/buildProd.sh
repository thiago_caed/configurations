#!/usr/bin/env bash

REPOSITORY=servers;
IMAGE_NAME=logstash;
IMAGETAG=PROD-1.0.4;

#Build
docker build -t ccr.caeddigital.net/repositorio/$REPOSITORY/$IMAGE_NAME .
docker tag ccr.caeddigital.net/repositorio/$REPOSITORY/$IMAGE_NAME ccr.caeddigital.net/repositorio/$REPOSITORY/$IMAGE_NAME:$IMAGETAG

#Login
docker login -u $1 --password-stdin $2 ccr.caeddigital.net

docker push ccr.caeddigital.net/repositorio/$REPOSITORY/$IMAGE_NAME:$IMAGETAG