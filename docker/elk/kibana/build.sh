#!/usr/bin/env bash

#Build
docker build -t ccr.caeddigital.net/repositorio/servers/kibana:1.0.0 .
docker tag ccr.caeddigital.net/repositorio/servers/kibana:1.0.0  ccr.caeddigital.net/repositorio/servers/kibana:1.0.0

#Login
docker login -u $1 --password-stdin $2 ccr.caeddigital.net

docker push ccr.caeddigital.net/repositorio/servers/kibana:1.0.0