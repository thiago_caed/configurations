#!/usr/bin/env bash

docker rm -f elasticsearch

docker run \
    --name elasticsearch \
    -p 9200:9200 \
    -p 9300:9300 \
    -v /var/opt/containers/service-bus-hmg-v2/elasticsearch/data:/usr/share/elasticsearch/data/nodes/0 \
    ccr.caeddigital.net/repositorio/servers/elasticsearch:1.0.4
