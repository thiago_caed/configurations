#!/usr/bin/env bash

#Build
docker build -t ccr.caeddigital.net/repositorio/servers/elasticsearch:1.0.4 .

#Login
docker login -u $1 --password-stdin $2 ccr.caeddigital.net

docker push ccr.caeddigital.net/repositorio/servers/elasticsearch:1.0.4