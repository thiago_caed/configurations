#!/usr/bin/env bash

#Build
docker build -t ccr.caeddigital.net/repositorio/servers/zookeeper .
docker tag ccr.caeddigital.net/repositorio/servers/zookeeper ccr.caeddigital.net/repositorio/servers/zookeeper:1.0

#Login
docker login -u $1 --password-stdin $2 ccr.caeddigital.net

docker push ccr.caeddigital.net/repositorio/servers/zookeeper:1.0