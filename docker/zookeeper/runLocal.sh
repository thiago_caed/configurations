#!/usr/bin/env bash

docker run -d \
    --name=zookeeper-1 \
    --net=host \
    -e ZOOKEEPER_SERVER_ID=1 \
    -e ZOOKEEPER_TICK_TIME='2000' \
    -e ZOOKEEPER_CLIENT_PORT='12181' \
    -e JAVA_TOOL_OPTIONS='-Xms512m -Xmx512m' \
    -e ZOOKEEPER_SERVERS="localhost:22888:23888;localhost:32888:33888" \
    --restart=always \
    ccr.caeddigital.net/repositorio/servers/zookeeper:1.0
# -p 12181:12181 \
#ZOOKEEPER_SERVERS: server.1=0.0.0.0:2888:3888 server.2=zoo2:2888:3888

docker run -d \
    --name=zookeeper-2 \
    --net=host \
    -e ZOOKEEPER_SERVER_ID=2 \
    -e ZOOKEEPER_TICK_TIME='2000' \
    -e ZOOKEEPER_CLIENT_PORT='22181' \
    -e JAVA_TOOL_OPTIONS='-Xms512m -Xmx512m' \
    -e ZOOKEEPER_SERVERS="localhost:22888:23888;localhost:32888:33888" \
    --restart=always \
    ccr.caeddigital.net/repositorio/servers/zookeeper:1.0
#ZOOKEEPER_SERVERS: server.1=zoo1:2888:3888 server.2=0.0.0.0:2888:3888
# -p 22181:22181 \
