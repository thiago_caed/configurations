#!/usr/bin/env bash

docker build -t ccr.caeddigital.net/repositorio/servers/openjdk8:1.0.0 .

docker login -u $1 --password-stdin $2 ccr.caeddigital.net

docker push ccr.caeddigital.net/repositorio/servers/openjdk8:1.0.0
